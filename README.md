## Order Match Service

-   Each client will have its own instance of the orderbook.
-   A worker and a client is created for a user. This is controlled by passing `userId` while starting the service.
-   Uses Grenache for communication.
-   To handle race condition, a helper class has been used to ensure that the same order is not being accessed by multiple users.

### Technical details

-   `client/client.js` - Creates an instance for a client
-   `server/index.js` - Creates an order book for the user and handles all the order details
-   `order/orderBook.js` - Main Class file to handle the order details of a user. A class in instantiated for every user.
-   `order/orderLock.js` - Singleton class to handle locked status for every order.

### Pre-requisites

-   Run `npm i -g grenache-grape` to install Grenache globally
-   On a terminal, run `grape --version` to verify Greanape grape is installed properly
-   Start two grapes and connect them

```
    grape --dp 20001 --aph 30001 --bn '127.0.0.1:20002'
    grape --dp 20002 --aph 40001 --bn '127.0.0.1:20001'
```

### Steps to Run

-   Clone the repository and navigate to the folder destination.
-   Create `.env` file and enter the following

```
    HOST=http://127.0.0.1:30001
```

-   Run `yarn install` to install all the dependencies
-   To instantiate a user, run the following commands

```
  node worker/index.js <userId>
  node client/client.js <userId>
```

This will run the hardcoded orders.
