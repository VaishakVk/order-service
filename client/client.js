require("dotenv").config();
const { PeerRPCClient } = require("grenache-nodejs-http");
const Link = require("grenache-nodejs-link");
const { constants } = require("../constants");

// UserId will denote the user to which the client is connected
const userId = process.argv[2];
if (!userId) {
    console.error("User ID is not passed");
    process.exit(1);
}

const link = new Link({
    grape: process.env.HOST,
});
link.start();

const peer = new PeerRPCClient(link, {});
peer.init();

// Hardcoded orders
const orders = [
    { action: constants.Action.SELL, stock: "ABC", quantity: 2 },
    { action: constants.Action.BUY, stock: "XYZ", quantity: 2 },
    { action: constants.Action.BUY, stock: "ABC", quantity: 2 },
    { action: constants.Action.SELL, stock: "XYZ", quantity: 2 },
];

const formatResponse = (data) => {
    const status = {
        fulfilled: false,
        notFulfilled: false,
    };
    for (res of data) {
        if (res.msg == constants.Response.FULFILLED) {
            status.fulfilled = true;
        } else {
            status.notFulfilled = true;
        }
    }

    if (status.fulfilled) return "Order fulfilled";
    else return "Order Accepted";
};

for (let i = 0; i < orders.length; i++) {
    const payload = {
        action: orders[i].action,
        stock: orders[i].stock,
        quantity: orders[i].quantity,
        userId,
    };
    // Used peer.map to send it to all the peers
    peer.map(`order_book`, payload, { timeout: 10000 }, (err, data) => {
        if (err) {
            console.error("Errored at ", err);
        }
        console.log(formatResponse(data));
    });
}
