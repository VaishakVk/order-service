require("dotenv").config();
const { PeerRPCServer } = require("grenache-nodejs-http");
const Link = require("grenache-nodejs-link");
const { constants } = require("../constants");
const { OrderBook } = require("../order/orderBook");

// UserId will denote the user to which the server is connected
const userId = process.argv[2];
if (!userId) {
    console.error("User ID is not passed");
    process.exit(1);
}

const link = new Link({
    grape: process.env.HOST,
});
link.start();

const peer = new PeerRPCServer(link, {
    timeout: 300000,
});
peer.init();

const port = 3000 + Math.floor(Math.random() * 1000);

const service = peer.transport("server");
service.listen(port);

setInterval(function () {
    link.announce(`order_book`, service.port, {});
}, 1000);

const orderBook = new OrderBook(userId);

service.on("request", (_, _1, payload, handler) => {
    const { action, stock, quantity, userId: userReqId } = payload;
    if (userReqId == userId) {
        orderBook.createOrder(action, stock, quantity);
        handler.reply(null, { msg: constants.Response.CREATED });
    } else {
        isOrderFullfilled = orderBook.matchOrder(
            action,
            stock,
            quantity,
            userReqId
        );
        let msg;
        if (isOrderFullfilled) {
            msg = constants.Response.FULFILLED;
        } else {
            msg = constants.Response.NOT_FULFILLED;
        }
        handler.reply(null, { msg });
    }
});
