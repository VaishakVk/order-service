const constants = Object.freeze({
    Action: {
        BUY: "BUY",
        SELL: "SELL",
    },
    LockStatus: {
        LOCKED: "LOCKED",
        AVAILABLE: "AVAILABLE",
        FULFILLED: "FULFILLED",
    },
    OrderStatus: {
        PENDING: "PENDING",
        COMPLETE: "COMPLETE",
    },
    Response: {
        FULFILLED: "Order fulfilled",
        NOT_FULFILLED: "Order not fulfilled",
    },
});

module.exports = {
    constants,
};
