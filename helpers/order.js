/**
 * @name createOrderId
 * @description Creates an order Id based on current date and a random number
 * @returns string
 */
const createOrderId = () => {
    return new Date().getTime() + "_" + Math.floor(Math.random() * 100000);
};

module.exports = {
    createOrderId,
};
