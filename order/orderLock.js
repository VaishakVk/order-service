const { constants } = require("../constants");

/**
 * Singleton Class to handle the lock instance
 */
class OrderLock {
    orderLockStatus = {};
    constructor() {
        if (OrderLock._instance) {
            return OrderLock._instance;
        }
        OrderLock._instance = this;
    }
    /**
     * @description Get order lock status
     * @param {string} orderId
     * @returns string
     */
    getOrderStatus(orderId) {
        return this.orderLockStatus[orderId];
    }

    /**
     * @description Acquire order lock for the orderIds
     * @param {[]string} orderIds
     * @returns boolean
     */
    acquireOrderLock(orderIds) {
        const isLockAvailable = orderIds.every(
            (orderId) =>
                this.orderLockStatus[orderId] == constants.LockStatus.AVAILABLE
        );
        if (!isLockAvailable) return false;
        orderIds.forEach((orderId) => {
            this.orderLockStatus[orderId] = constants.LockStatus.LOCKED;
        });
        return true;
    }

    /**
     * @description Release order locks
     * @param {[]string} orderIds
     */
    releaseOrderLock(orderIds) {
        orderIds.forEach((orderId) => {
            this.orderLockStatus[orderId] = constants.LockStatus.AVAILABLE;
        });
    }
}

module.exports = {
    OrderLock,
};
