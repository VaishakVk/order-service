const { createOrderId } = require("../helpers/order");
const { constants } = require("../constants");
const { OrderLock } = require("./orderLock");

/**
 * Order Book - created for each user
 */
class OrderBook {
    /**
     * Map to track all the orders
     * {
     *    "ABC": {
     *        "BUY": [],
     *        "SELL": []
     *     }
     * }
     */
    orders = {};
    constructor(userId) {
        this.userId = userId;
    }

    /**
     * @description Generates the data for order
     * @param {string} stock
     * @param {int} quantity
     * @returns object
     */
    createOrderPacket(stock, quantity) {
        const id = createOrderId();
        return { key: stock, value: { quantity, id } };
    }

    /**
     * @description If BUY then return SELL; if SELL return BUY
     * @param {string} action
     * @returns string
     */
    _getCounterAction(action) {
        let actionToCheck;
        if (action == constants.Action.BUY) {
            actionToCheck = constants.Action.SELL;
        } else {
            actionToCheck = constants.Action.BUY;
        }
        return actionToCheck;
    }

    /**
     * @description Gets all the pending orders
     * @param {string} stock
     * @param {string} action
     * @returns object
     */
    getPendingOrdersForStock(stock, action) {
        const actionToCheck = this._getCounterAction(action);
        const orderDetails = this.orders[stock][actionToCheck];
        if (!orderDetails) this.orders[stock][actionToCheck] = [];
        const orderIds = [];
        const result = [];
        orderDetails.forEach((order) => {
            if (order.status == constants.OrderStatus.PENDING) {
                orderIds.push(order.id);
                result.push(order);
            }
        });

        return { orderDetails: result, orderIds };
    }

    /**
     * @description Reduces quantity from existing pending orders and completes the order
     * @param {string} stock
     * @param {int} quantity
     * @param {string} action
     * @param {string} id
     */
    _settleOrder(stock, quantity, action, id) {
        const orders = [...this.orders[stock][action]];
        for (const order of orders) {
            if (quantity > 0 && order.status == constants.OrderStatus.PENDING) {
                const quantityToReduce = Math.min(quantity, order.quantity);
                quantity -= quantityToReduce;

                if (order.quantity == quantityToReduce) {
                    this.completeOrder(action, order.quantity, id);
                }
            }
        }
    }

    /**
     * @description Creates an entry to orders as pending
     * @param {string} action
     * @param {string} stock
     * @param {int} quantity
     */
    createOrder(action, stock, quantity) {
        if (!this.orders[stock])
            this.orders[stock] = {
                [constants.Action.SELL]: [],
                [constants.Action.BUY]: [],
            };
        const orderDetails = this.createOrderPacket(stock, quantity);
        this.orders[stock][action].push(orderDetails.value);
    }

    /**
     * @description Updates the order status as COMPLETE
     * @param {string} id
     * @param {string} stock
     * @param {string} action
     */
    completeOrder(id, stock, action) {
        for (let i = 0; i < this.orders[stock][action].length; i++) {
            if (this.orders[stock][action][i].id == id) {
                this.orders[stock][action][i].status ==
                    constants.OrderStatus.COMPLETE;
            }
        }
    }

    /**
     * @description Checks for existing buy/sell orders and matches them
     * @param {string} action
     * @param {string} stock
     * @param {int} quantity
     * @param {string} userId
     * @returns
     */
    matchOrder(action, stock, quantity, userId) {
        if (this.userId == userId) {
            return false;
        }
        const orderLock = new OrderLock();

        // If the order is already fulfilled return false
        if (orderLock.getOrderStatus(userId) == constants.LockStatus.FULFILLED)
            return false;

        // If user doesnt have the stock return false
        if (!this.orders[stock]) return false;

        const { orderDetails, orderIds } = this.getPendingOrdersForStock(
            stock,
            action
        );
        if (!orderDetails || orderDetails.length) return false;

        // Check if the order is available for prrocessing
        const orderLockObtained = orderLock.acquireOrderLock(orderIds);

        // If lock exists, then retry the function
        if (!orderLockObtained)
            this.matchOrder(action, stock, quantity, userId);

        orderIds.map((id) =>
            this._settleOrder(
                stock,
                quantity,
                this._getCounterAction(action),
                id
            )
        );

        // Release order lock
        orderLock.releaseOrderLock(orderIds);
        return true;
    }
}

module.exports = {
    OrderBook,
};
